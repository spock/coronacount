# coronacount - Pokaż liczbę aktualnie zainfekowanych COVID-19 w PL

## Opis

Aplikacja wrzucona na prośbę paru znajomych :)
Trzeba podać URL do API na arcgis.com mając wcześniej tam konto lub znając odpowiedni URL :P
Jesli chcemy wsparcie dla opcji wysyłania powiadomień via pushover nalezy uzupełnić dwie zmienne w ramach swojego konta.

### Instalacja
Pobierz kod:
```
git clone gitlab.com/spock/coronacount
```

Zainstaluj zależności:
```
pip3 install -r requirements.txt
```

### Użycie

Aplikacja posiada przełącznik "-h"
```
spock@fx160 ~/coronacount> ./coronacount.py
Ilość infekcji: 221

spock@fx160 ~/coronacount> ./coronacount.py -v
Województwo lodzkie: 42
Województwo mazowieckie: 39
Województwo dolnoslaskie: 30
Województwo slaskie: 25
Województwo lubelskie: 21
Województwo wielkopolskie: 11
Województwo podkarpackie: 11
Województwo opolskie: 8
Województwo kujawsko-pomorskie: 8
Województwo warminsko-mazurskie: 7
Województwo zachodniopomorskie: 6
Województwo swietokrzyskie: 4
Województwo malopolskie: 4
Województwo pomorskie: 3
Województwo lubuskie: 2
Województwo podlaskie: 0
Ilość infekcji: 221

```



