#!/usr/bin/env python3
import requests
import lxml.html as lh
import argparse
import sys
from pushover import Client
from datetime import datetime

#GLOBAL
## If you want pushover notifications
PUSHOVER_USER_KEY=""
PUSHOVER_TOKEN=""
PUSHOVER_MSG_TEMPLATE="Ilośc infekcji: {}"
## Data source to parse
DATA_URL="https://pl.m.wikipedia.org/wiki/Pandemia_wirusa_SARS-CoV-2_w_Polsce"


def send_pushover(topic,msg):
    try:
        client = Client(PUSHOVER_USER_KEY, api_token=PUSHOVER_TOKEN)
        client.send_message(msg,topic )
    except:
        pass

# check if Do Not Disturbe time is active
def is_dnd(start,end):
     now = datetime.now()
     if now.hour <= end:
         return start <= now.hour <= end
     else:
         return start <= now.hour or x <= end

def get_data(url):
    page = requests.get(DATA_URL)
    doc = lh.fromstring(page.content)
    tb_elements = doc.xpath("//table[@class='infobox']/tbody/tr")
    if len(tb_elements) > 8:
        cnt = tb_elements[8].findall("td")
        if len(cnt) == 2:
            return cnt[1].text
        else:
            return None

def main(args):
    records = get_data(DATA_URL)
    #total_infection = records[-1].split(":")[1].strip()
    total_infection = records
    if not args.quiet:
        print("Ilość infekcji SARS-CoV-2: {0}".format(total_infection))
    #if args.verbose:
    #    for record in records:
    #        print(record)
    if args.pushover:
        send_pushover("Koronawirus update", PUSHOVER_MSG_TEMPLATE.format(total_infection))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #parser.add_argument("-v","--verbose", help="Wyświetls histogram przyrostu po dacie",action="store_true")
    parser.add_argument("-p","--pushover", help="Wyślij jako powiadomienie pushover",action="store_true")
    parser.add_argument("-q","--quiet", help="Nie wyświetlaj nic na konsoli (opcja dla pushover/cron)",action="store_true")
    args = parser.parse_args()
    main(args)

